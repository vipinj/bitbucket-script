#Vipin Jain, Script to interface bitbucket
#!/bin/bash

choice=0;
while [ $choice -ne 5 ]
do
    echo -e "\nPlease select your choice:"
    echo "Create a Repo [1]."
    echo "Delete a Repo [2]."
    echo "Clone a Repo [3]."
    echo "Set commit notifications[4]."
    echo "Exit[5]."
    read choice
    if [ "$choice" -ne "5" ]
    then
	if [ "$username" == "" ]
	then
	    echo "Please enter the username:"
	    read -s username
#echo $username
	    echo "Please enter the password:"
	    read -s password
	fi
#echo $password
    fi
    if [ "$choice" -eq "1" ]
    then
	echo "Please enter the repo-name:"
	read repo
#echo $repo
	echo "git/hg?:"
	read type
#echo $type
	echo "Private(y)/Public(n):"
	read access
	if [ "$access" == "y" ]
	then
	    curl --request POST --user $username:$password https://api.bitbucket.org/1.0/repositories/ --data name=$repo --data scm=$type --data is_private=true
	else
	    curl --request POST --user $username:$password https://api.bitbucket.org/1.0/repositories/ --data name=$repo --data scm=$type 
	fi
    elif [ "$choice" -eq "2" ]
    then
#	echo -e "\n Do you want to set up commit email(Diff)(y/n)"
#	read choice2
#	if [ "$choice2" == "y" ]
#	then
	echo "Please enter the repo name to be deleted"
	read reponame
	curl --request DELETE --user $username:$password https://api.bitbucket.org/1.0/repositories/$username/$reponame/
    elif [ "$choice" -eq "3" ]
    then
	echo "Please enter the repo name to be cloned"
	read reponame
	git clone https://bitbucket.org/$username/$reponame.git
    elif [ "$choice" -eq "4" ]
    then    
	echo "Please enter your email"
	read email
#    echo $email
	curl --request POST --user $username:$password https://api.bitbucket.org/1.0/repositories/$username/$repo/services/ --data "type=Email Diff;Email=$email" 
    elif [ "$choice" -eq "5" ]
    then
	exit
    else
	exit
    fi
done